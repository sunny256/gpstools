<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <!-- doc/custom.xsl -->
  <!-- File ID: 2c8c8db0-54b0-11df-9c34-90e6ba3022ac -->
  <xsl:import href="/usr/share/xml/docbook/stylesheet/docbook-xsl/manpages/docbook.xsl"/>
  <xsl:param name="local.l10n.xml" select="document('')"/>
  <l:i18n xmlns:l="http://docbook.sourceforge.net/xmlns/l10n/1.0">
    <l:l10n language="en">

      <!-- Get rid of annoying US date format -->
      <l:context name="datetime">
        <l:template name="format" text="Y-m-d"/>
      </l:context>

    </l:l10n>
  </l:i18n>
</xsl:stylesheet>
