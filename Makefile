# Makefile for gpstools
# File ID: 32685082-4ea4-11df-a951-90e6ba3022ac

.PHONY: default
default:
	cd doc && $(MAKE)

.PHONY: clean
clean:
	cd doc && $(MAKE) clean
	cd tests && $(MAKE) clean

.PHONY: test
test:
	cd tests && $(MAKE) test
	cd doc && $(MAKE) test

.PHONY: remotes
remotes:
	git remote add Spread sunny@git.sunbase.org:/home/sunny/Git-spread/gpstools.git; true
	git remote add bitbucket git@bitbucket.org:sunny256/gpstools.git; true
	git remote add github git@github.com:sunny256/gpstools.git; true
	git remote add gitlab git@gitlab.com:sunny256/gpstools.git; true
	git remote add repoorcz ssh://sunny256@repo.or.cz/srv/git/gpstools.git; true
	git remote add sunbase sunny@git.sunbase.org:/home/sunny/Git/gpstools; true
