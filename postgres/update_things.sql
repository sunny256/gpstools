-- update_things.sql
-- File ID: 4765dc0a-fafb-11dd-874c-000475e441b9

-- Round waypoints to six decimal digits -- {{{
\echo
\echo ================ Round waypoints to six decimal digits ================

UPDATE wayp SET coor = point(
    round(coor[0]::numeric, 6),
    round(coor[1]::numeric, 6)
);
-- }}}
-- Remove duplicates from wayp -- {{{
\echo
\echo ================ Remove duplicates from wayp ================

COPY (SELECT '======== Number of waypoints in wayp before cleanup: ' || count(*) from wayp) to STDOUT;

BEGIN ISOLATION LEVEL SERIALIZABLE;
    CREATE TEMPORARY TABLE dupfri
    ON COMMIT DROP
    AS (
        SELECT
            DISTINCT ON (
                coor[0], coor[1],
                name,
                ele,
                type,
                time,
                cmt,
                descr,
                src,
                sym
            ) *
            FROM wayp
    );
    TRUNCATE wayp;
    INSERT INTO wayp (
        SELECT *
            FROM dupfri
            ORDER BY name
    );
COMMIT;

COPY (SELECT '======== Number of waypoints in wayp after cleanup: ' || count(*) from wayp) to STDOUT;
-- }}}

-- Remove duplicates from events -- {{{
\echo
\echo ================ Remove duplicates from events ================

COPY (SELECT '======== Event count before cleanup: ' || count(*) from events) to STDOUT;

BEGIN ISOLATION LEVEL SERIALIZABLE;
    CREATE TEMPORARY TABLE dupfri
    ON COMMIT DROP
    AS (
        SELECT
            DISTINCT ON (date, coor[0], coor[1], descr) *
            FROM events
    );
    TRUNCATE events;
    INSERT INTO events (
        SELECT *
            FROM dupfri
            ORDER BY date
    );
COMMIT;

COPY (SELECT '======== Event count after cleanup: ' || count(*) from events) to STDOUT;
-- }}}

-- Update picture coordinates -- {{{
\echo
\echo ================ Update picture coordinates ================

UPDATE pictures SET coor = findpos(date)
    WHERE coor IS NULL;
-- }}}
-- Round picture coordinates -- {{{
\echo ================ Round picture coordinates ================
UPDATE pictures SET coor = point(
    round(coor[0]::numeric, 6),
    round(coor[1]::numeric, 6)
);
-- }}}
-- Remove duplicates from pictures -- {{{
\echo
\echo ================ Remove duplicates from pictures ================

COPY (SELECT '======== Picture count before cleanup: ' || count(*) from pictures) to STDOUT;

BEGIN ISOLATION LEVEL SERIALIZABLE;
    CREATE TEMPORARY TABLE dupfri
    ON COMMIT DROP
    AS (
        SELECT
            DISTINCT ON (date, coor[0], coor[1], descr, filename, author) *
            FROM pictures
    );
    TRUNCATE pictures;
    INSERT INTO pictures (
        SELECT *
            FROM dupfri
            ORDER BY date
    );
COMMIT;

COPY (SELECT '======== Picture count after cleanup: ' || count(*) from pictures) to STDOUT;
-- }}}

-- Update coordinates for films -- {{{
\echo
\echo ================ Update coordinates for films ================

UPDATE film SET coor = findpos(date)
    WHERE coor IS NULL;
-- }}}
-- Round film coordinates -- {{{
\echo ================ Round film coordinates ================
UPDATE film SET coor = point(
    round(coor[0]::numeric, 6),
    round(coor[1]::numeric, 6)
);
-- }}}
-- Remove duplicates from film -- {{{
\echo
\echo ================ Remove duplicates from film ================

COPY (SELECT '======== Film count before cleanup: ' || count(*) from film) to STDOUT;

BEGIN ISOLATION LEVEL SERIALIZABLE;
    CREATE TEMPORARY TABLE dupfri
    ON COMMIT DROP
    AS (
        SELECT
            DISTINCT ON (date, coor[0], coor[1], descr, filename, author) *
            FROM film
    );
    TRUNCATE film;
    INSERT INTO film (
        SELECT *
            FROM dupfri
            ORDER BY date
    );
COMMIT;

COPY (SELECT '======== Film count after cleanup: ' || count(*) from film) to STDOUT;
-- }}}

-- Oppdater koordinater for lyd -- {{{
\echo
\echo ================ Update sound coordinates ================

UPDATE lyd SET coor = findpos(date)
    WHERE coor IS NULL;
-- }}}
-- Update sound coordinates -- {{{
\echo ================ Update sound coordinates ================
UPDATE lyd SET coor = point(
    round(coor[0]::numeric, 6),
    round(coor[1]::numeric, 6)
);
-- }}}
-- Remove duplicates from sound -- {{{
\echo
\echo ================ Remove duplicates from sound ================

COPY (SELECT '======== Sound count before cleanup: ' || count(*) from lyd) to STDOUT;

BEGIN ISOLATION LEVEL SERIALIZABLE;
    CREATE TEMPORARY TABLE dupfri
    ON COMMIT DROP
    AS (
        SELECT
            DISTINCT ON (date, coor[0], coor[1], descr, filename, author) *
            FROM lyd
    );
    TRUNCATE lyd;
    INSERT INTO lyd (
        SELECT *
            FROM dupfri
            ORDER BY date
    );
COMMIT;

COPY (SELECT '======== Sound count after cleanup: ' || count(*) from lyd) to STDOUT;
-- }}}

\i distupdate.sql
\i hours.sql
