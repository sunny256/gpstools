-- round_coor.sql
-- File ID: 46f9e76a-56db-11df-adc3-ec9b5bd3a10b

-- Round the trackpoint log to six decimal digits -- {{{
\echo
\echo ================ Round trackpoint coordinates to six decimal digits ================

UPDATE logg SET coor = point(
    round(coor[0]::numeric, 6),
    round(coor[1]::numeric, 6)
);
-- }}}
